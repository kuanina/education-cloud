package com.education.cloud.system.service.feign.biz;

import com.education.cloud.system.service.dao.SysMenuRoleDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.education.cloud.system.feign.qo.SysMenuRoleQO;
import com.education.cloud.system.feign.vo.SysMenuRoleVO;
import com.education.cloud.system.service.dao.impl.mapper.entity.SysMenuRole;
import com.education.cloud.system.service.dao.impl.mapper.entity.SysMenuRoleExample;
import com.education.cloud.system.service.dao.impl.mapper.entity.SysMenuRoleExample.Criteria;
import com.education.cloud.util.base.Page;
import com.education.cloud.util.base.PageUtil;
import com.education.cloud.util.tools.BeanUtil;

/**
 * 菜单角色关联表
 *
 * @author wujing
 */
@Component
public class FeignSysMenuRoleBiz {

	@Autowired
	private SysMenuRoleDao dao;

	public Page<SysMenuRoleVO> listForPage(SysMenuRoleQO qo) {
		SysMenuRoleExample example = new SysMenuRoleExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<SysMenuRole> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, SysMenuRoleVO.class);
	}

	public int save(SysMenuRoleQO qo) {
		SysMenuRole record = BeanUtil.copyProperties(qo, SysMenuRole.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public SysMenuRoleVO getById(Long id) {
		SysMenuRole record = dao.getById(id);
		return BeanUtil.copyProperties(record, SysMenuRoleVO.class);
	}

	public int updateById(SysMenuRoleQO qo) {
		SysMenuRole record = BeanUtil.copyProperties(qo, SysMenuRole.class);
		return dao.updateById(record);
	}

}
