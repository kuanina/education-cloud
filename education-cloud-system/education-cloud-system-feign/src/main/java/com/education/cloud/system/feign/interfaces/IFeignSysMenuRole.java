package com.education.cloud.system.feign.interfaces;

import com.education.cloud.system.feign.qo.SysMenuRoleQO;
import com.education.cloud.system.feign.vo.SysMenuRoleVO;
import com.education.cloud.util.base.Page;
import com.education.cloud.util.constant.ServiceConstant;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 菜单角色关联表
 *
 * @author wujing
 */
@FeignClient(name = ServiceConstant.SYSTEM_SERVICE,contextId = "sysMenuRoleClient")
public interface IFeignSysMenuRole {

    @RequestMapping(value = "/feign/system/sysMenuRole/listForPage")
    Page<SysMenuRoleVO> listForPage(@RequestBody SysMenuRoleQO qo);

    @RequestMapping(value = "/feign/system/sysMenuRole/save")
    int save(@RequestBody SysMenuRoleQO qo);

    @RequestMapping(value = "/feign/system/sysMenuRole/deleteById")
    int deleteById(@RequestBody Long id);

    @RequestMapping(value = "/feign/system/sysMenuRole/updateById")
    int updateById(@RequestBody SysMenuRoleQO qo);

    @RequestMapping(value = "/feign/system/sysMenuRole/getById")
    SysMenuRoleVO getById(@RequestBody Long id);

}
