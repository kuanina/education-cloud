package com.education.cloud.util.constant;

/**
 * @Description 系统常量类
 * @Date 2020/3/22
 * @Created by 67068
 */
public interface Constants {

    /**
     * 用户编号
     */
     String USERNO = "userNo";

    /**
     * 用户token
     */
     String TOKEN = "token";

    /**
     * Gateway请求头TOKEN名称（不要有空格）
     */
     String GATEWAY_TOKEN_HEADER = "gatewaytoken";
    /**
     * Gateway请求头TOKEN值
     */
    String GATEWAY_TOKEN_VALUE = "gateway:123456";


     String ADMIN = "admin";// admin
     Integer FREEZE = 3;// 冻结状态
     String REGEX_MOBILE = "^1[0-9]{10}$";

    /**
     * session
     *
     * @author wujing
     */
     interface Session {
         String BOSS_MENU = "BOSS_MENU";//
         String USER_NO = "USERNO"; // userno
         String USER_VO = "USERVO";// 不能使用user，关键词
         String REAL_NAME = "REALNAME";//
    }

    /**
     * cookie
     *
     * @author wujing
     */
     interface Cookie {
         String USER_TOKEN = "USERTOKEN";
    }

    /**
     * 日期类型
     *
     * @author wujing
     */
     interface DATE {
         String YYYYMMDD = "yyyyMMdd";
         String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
         String YYYYMMDDHHMMSSSSS = "yyyyMMddHHmmssSSS";
         String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
         String YYYY_MM_DD = "yyyy-MM-dd";
    }
}
